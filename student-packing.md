Packing List for Students
===

**comfort:** pajamas/robe/slippers, chair, blanket, pillow, stuffed llama

**clothing:** for cool and warm weather

**laptop:** 2+ cores, 8+ GiB memory, 16+ GiB *available* storage, VirtualBox

**notebook/sketchbook:** note-taking / drawing supplies

**toiletries**

* toothbrush and toothpaste
* deodorant
* shampoo
* soap
* towel

**toys**

* hiking shoes
* mountain bike
* swimsuit
* musical instrument
* game
* movie
* molecule(s): caffeine, alcohol, nicotine...
