# Service-Oriented Architecture

```
From: Sarah <cto@house-of-py.com>
To: Peyton <devops@house-of-py.com>
Cc: Ada <development@house-of-py.com>
Subject: Start Down the Docker Road

Peyton & Ada,
I've really been wanting to go a service-oriented route with our architecture.  
One of the advisors on our board, Eli, suggested that we look in to Docker to
help us out with this.  Will you please switch the demo app to use an external
database running in Docker?

-Sarah

```

---

A.K.A. Microservices*

#### What are we going to do in this lab?

The Django app is self-contained right now.  We're going to create a PostgreSQL database to store our application's data.

#### Why do we need to do this?

- Simpler distributed coding
- Allows us to manage stateful data as a service
- We can manage bottlenecks independently
- Isolated impacts

#### When do you use it?

Many applications start life as a monolith.  At some point, there might be a particular function within an app that could be used elsewhere.

There are some basic principles - like separating the database from the application - that are much easier to follow up-front.

---

### To configure a Docker host to use the DevOps Camp mirror (to speed image downloads)

```bash
REGISTRY_IP=10.5.5.10 # Replace with the actual IP of the local Docker hub cache

echo '{
  "registry-mirrors": ["http://'$REGISTRY_IP':5000"]
}' | sudo tee /etc/docker/daemon.json

sudo systemctl stop docker
sudo systemctl start docker
```

---

PROBLEM: Our app uses a SQLite DB to store stateful data.  We need to move it.

Let's use PostgreSQL!

1. Generate a "random" password for Postgres:
  - `export POSTGRES_PASSWORD=$(pwgen -y 16 1)`
1. Create a Docker volume that will hold persistent data:
  - `docker volume create pgsql_data`
1. Run a Postgres server
  - `docker run -d --rm --name db -v pgsql_data:/var/lib/postgresql/data -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD -p 5432:5432 postgres:10.4`
  - Same-same (but test it): `docker run -d --rm --name db -v pgsql_data:/var/lib/postgresql/data -e POSTGRES_PASSWORD -p 5432:5432 postgres:10.4`

Change the Django settings

1. Edit the `mysite/mysite/settings.py` file
1. Comment out the sqlite block
1. Uncomment the Postgres block

Add the appropriate environment variables for the webapp

```
export DJANGO_DB_USER=postgres
export DJANGO_DB_PASS=$POSTGRES_PASSWORD
export DJANGO_DB_HOST=localhost
```

Try to run the app:

1. `python3 manage.py runserver`
1. [Check it out](http://localhost:8000/polls)
1. Uh-oh!  There's no data!
1. `python3 manage.py migrate`

Set up our stuff (again. ugh.)
1. `python3 manage.py createsuperuser`
