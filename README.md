# DevOps Camp

---

Welcome to SFS' DevOps Camp git repo

This material is designed to create realistic scenarios to teach the skills to be a DevOps Engineer.

[labs](labs/)

---

## The One Thing

- Pipeline

---

#### What are we going to do in this Camp?

We're going to give you the skills you need to be an effective DevOps Engineer!

#### Why do we need to do this?

- Go faster
  - Speed up feature delivery with multiple deploys per day
- Stay safe
  - Integrate automated testing to squash bugs before they get to production
- Web scale
  - Don't stress when a system gets overloaded.  Just scale it.
- Uptime
  - Build fault-tolerant, self-healing systems
- Efficiency
  - Manage hundreds - maybe thousands - of services

#### When do you use it?

DevOps practices can be applied to just about every stage of the software lifecycle.  

- Planning and architecture
- Build and test
- Deploy and monitor

---

#### Not All Sunshine and Roses

- Iterations
  - 100+ CI builds to get a headless click test working
  - ~25 is more common
- Highlight the pain
- Show; Don't Tell

> “A good plan, violently executed now, is better than a perfect plan next week.”
- Gen George S. Patton

[CC BY SA](LICENSE)
