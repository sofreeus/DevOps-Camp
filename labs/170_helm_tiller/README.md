### Prepare for Tiller

```
From: Eli <guru@house-of-py.com>
To: Peyton <devops@house-of-py.com>
Subject: The New Configuration Management

Hello Peyton,
There's a very common configuration management practice known as "library +
wrapper" or "generic + application."  This is where you take an off-the-shelf
"library" config (e.g. Chef's Supermarket, Ansible Galaxy, Docker Hub), then
write your "wrapper" that only includes the necessary configuration to make your
app work the way you want.  For Kubernetes, there's a package manager called
Helm.  I think you should be able to get Helm set up for Kubernetes and do
something pretty similar.

http://devopsanywhere.blogspot.com/2012/11/how-to-write-reusable-chef-cookbooks.html

-Eli

```

---

You have two options here:

1. Examine and install (`kubectl apply`) the `tiller_prep.yaml` included with this lab.
1. Run these (pretty self-explanatory) commands manually
  - `kubectl create namespace tiller`
  - `kubectl --namespace tiller create serviceaccount tiller`
  - `kubectl create clusterrolebinding tiller-admin --clusterrole=cluster-admin --serviceaccount=tiller:tiller`

---

### Install Helm+Tiller

1. Install Helm on our VM
  - `sudo snap install helm`
1. Give Helm access to the kubectl configuration (Ubuntu)
  - `ln -s ~/.kube/config ~/snap/helm/common/kube/config`
  - This is not necessary on MacOS with `brew install kubernetes-helm`
1. Install Tiller
  - `helm init --tiller-namespace tiller --service-account tiller --wait`
  - The `--wait` is optional.  It will block the command until the Tiller install is complete.
1. Verify the installation
  - `helm version --tiller-namespace tiller`
1. Set your Tiller namespace option in your shell (for easier commanding)
  - `export TILLER_NAMESPACE=tiller`
  - More permanently: `echo export TILLER_NAMESPACE=tiller >> ~/.bashrc`
1. Verify the shell option is working
  - `helm version`

---

### Notes

- This is a good baseline for a development install, but any user on your network could run the Helm client and start installing things on your cluster.  The solution to this is [certificate-based authentication](https://github.com/helm/helm/blob/master/docs/tiller_ssl.md), which we don't have time to cover here.
- Again, for development we've granted Tiller admin-level access to the cluster resources.  In a production environment, you will want to investigate more [clusterrole](https://kubernetes.io/docs/reference/access-authn-authz/authentication/) options and make sure you use the right fit for your environment.
- Helm provides a [guide to securing your Helm installation](https://docs.helm.sh/using_helm/#securing-your-helm-installation), and DZone has a [list of pointers](https://dzone.com/articles/securing-helm), too.

---

### Configuration as Code!

Remember the GKE [configuration repo](git@gitlab.com:sofreeus/devopscamp-gke.git) that snuck in to the tail-end of the GKE lab?  That would be a grand place to store and document the Tiller install.

As a matter of fact, there is a `with_tiller` [branch](https://gitlab.com/sofreeus/devopscamp-gke/tree/with_tiller) that includes these commands!

---
