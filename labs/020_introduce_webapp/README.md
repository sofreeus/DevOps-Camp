# Our Django webapp

```
From: Sarah <cto@house-of-py.com>
To: Peyton <devops@house-of-py.com>
Cc: Ada <development@house-of-py.com>
Subject: A Template Webapp

Hi Peyton,
Ada's team is working on a webapp that we're going to use for one of our new
features.  I've copied her on this message, so you can pull her in if you need
help familiarizing yourself with it.

-Sarah

```

---

#### What are we going to do in this lab?

This lab introduces a Django webapp for creating and managing polls.  The application is a good example of a multi-tier webapp.

#### Why do we need to do this?

We're going to use this application to demonstrate DevOps concepts:
  - Build
  - Test
  - Package
  - Deploy
  - Run
  - Monitor

#### When do you use it?

If you're not familiar with Python+Django, perfect!  DevOps engineers don't have to understand everything about everything.  Well-understood patterns for good practices can be applied to almost any code/build process.

---

Use your repo setup script to create a new repo.  Pick a Macguffin.  Call the repo `${MACGUFFIN}-webapp`.

```
REPO_NAME=${MACGUFFIN}-webapp
repo_setup.sh ${REPO_NAME}
```

Take note of the `ssh_url_to_repo` value.

`REPO_URL=<<<ssh_url_to_repo>>>`

Clone the demo webapp into your repo, then change the remote git repository:

```
git clone git@gitlab.com:sofreeus/devopscamp-webapp.git ~/git/${REPO_NAME}
cd ~/git/${REPO_NAME}
git remote set-url origin ${REPO_URL}
```

Verify that your local copy of the repository is pointed at your freshly-minted GitLab repository:

`git remote -v`

Follow the instructions in the README.md to get the webapp running locally.
