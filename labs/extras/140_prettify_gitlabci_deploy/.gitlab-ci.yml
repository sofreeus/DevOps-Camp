image: docker:stable

services:
  - docker:dind

variables:
  POSTGRES_PASSWORD: ${CI_JOB_ID}
  DJANGO_DB_USER: postgres
  DJANGO_DB_PASS: ${CI_JOB_ID}
  DJANGO_DB_HOST: db

# SHORT_SHA is equivalent to what you see at
#   https://gitlab.com/<user|group>/<project>/commits/<branch>
# This helps us version the image as well as cross-reference
#   a production container with a specific git commit.
# GitLab CI won't double-dereference variables, so we
#  have to put these here instead of in `variables` above.
before_script:
  - export SHORT_SHA=$( echo ${CI_COMMIT_SHA} | cut -c -8 )

stages:
  - build_and_test
  - deploy_dev
  - deploy_qa

build_and_test:
  stage: build_and_test
  script:
    # Log in to the GitLab Container Registry
    - echo ${CI_BUILD_TOKEN} | docker login -u gitlab-ci-token --password-stdin registry.gitlab.com
    # GitLab CI won't double-dereference variables, so we have to put these here
    - export IMAGE_TAG="${CI_REGISTRY_IMAGE}:${SHORT_SHA}"
    - export LATEST_TAG="${CI_REGISTRY_IMAGE}:latest"
    # Make sure our code builds before we do anything else
    - docker build -t ${IMAGE_TAG} -t ${LATEST_TAG} .
    # Set up a network
    - docker network create localwebapp
    # Set up a DB instance
    - docker run --net=localwebapp -d --name db -e POSTGRES_PASSWORD:${POSTGRES_PASSWORD} postgres:10.4
    # Give PostgreSQL a change to start
    - sleep 5
    # Start our app
    - docker run --net=localwebapp -d --name webapp -p8000:8000 -e DJANGO_DB_USER=${DJANGO_DB_USER} -e DJANGO_DB_PASS=${DJANGO_DB_PASS} -e DJANGO_DB_HOST=${DJANGO_DB_HOST} ${CI_REGISTRY_IMAGE}
    - sleep 2
    # Migrate our models into the DB
    - docker exec webapp python manage.py migrate
    # Run our tests
    - docker exec webapp python manage.py test --debug-mode polls
    # Push the image to the registry
    - docker push ${CI_REGISTRY_IMAGE}

# A job that starts with a period is a "hidden job"
# The & in YAML is an "anchor" - kind of like defining a variable
.deploy_script: &deploy_script
  image: google/cloud-sdk:latest
  script:
    - echo ${CI_BUILD_TOKEN} | docker login -u gitlab-ci-token --password-stdin registry.gitlab.com
    - echo ${GCP_AUTH_KEYFILE} > /root/.config/keyfile.json
    - gcloud auth activate-service-account --key-file=/root/.config/keyfile.json
    - gcloud beta container clusters get-credentials ${CLUSTER} --region ${REGION} --project ${GCP_PROJECT}
    - mkdir manifests-${SHORT_SHA}
    - for FILE in $(ls k8s); do echo $FILE; sed -e "s|DOCKER_IMAGE|${IMAGE}|g" -e "s|DOMAIN|${DOMAIN}|g" k8s/$FILE > manifests-${SHORT_SHA}/$FILE; done
    - kubectl apply -f manifests-${SHORT_SHA}

deploy_to_dev:
  stage: deploy_dev
  environment:
    name: dev
  variables:
    GCP_AUTH_KEYFILE: ${DEV_GCP_AUTH_KEYFILE}
    CLUSTER: ${DEV_CLUSTER}
    REGION: ${DEV_REGION}
    PROJECT: ${DEV_PROJECT}
    DOMAIN: ${DEV_DOMAIN}
  <<: *deploy_script

deploy_to_prod:
  stage: deploy_prod
  environment:
    name: prod
  when: manual
  variables:
    GCP_AUTH_KEYFILE: ${PROD_GCP_AUTH_KEYFILE}
    CLUSTER: ${PROD_CLUSTER}
    REGION: ${PROD_REGION}
    PROJECT: ${PROD_PROJECT}
    DOMAIN: ${PROD_DOMAIN}
  <<: *deploy_script
