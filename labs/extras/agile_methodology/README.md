# Agile Methodology

---

### Why?

Traditional waterfall delivery

```

V  |
a  |
l  |                                   ____
u  |                                  |
e  |                                  |
   |                              ____|
D  |                             |
e  |                             |
l  |                             |
i  |                             |
v  |                             |
e  |                             |
r  |                             |
e  |                             |
d  |_____________________________|
   +--+--+--+--+--+--+--+--+--+--+--+--+--+--
   0  2  4  6  8 10 12 14 16 18 20 22 24 26
          Weeks of Development Time
```


Agile delivery

```

V  |                                   
a  |                                       
l  |                                    ___
u  |                                 __|
e  |                           _____|
   |                        __|
D  |                     __|
e  |                  __|
l  |               __|
i  |              |  
v  |            __|
e  |           |
r  |      _____|
e  |   __|
d  |__|
   +--+--+--+--+--+--+--+--+--+--+--+--+--+--
   0  2  4  6  8 10 12 14 16 18 20 22 24 26
          Weeks of Development Time
```

Things to Note
  - Agile delivery isn't perfectly smooth
  - Forming, norming, storming
  - Prioritization delivers higher-value features first
  - In-flight course-correct mitigates post-delivery rework
  - Compare the integrals
  - Because high-value work is completed sooner, it's easier to say, "We're going to forgo the rest of this and start a new project."
  - `Thin slices of functionality`

---

### The Basics

- Two-week sprints
- Quick (<15m) scrum every weekday morning
- Start the sprint with a planning meeting
- End the sprint with a demo (celebrate accomplishments)
- End the sprint with a retro
- 2-pizza teams

---

### Roles

- Product Manager
  - The product manager is usually someone on the business side.  This person is in contact with customers and has a holistic understanding of the market and its trends.
  - This person feeds requests and negotiates priorities with the product owner.
- Product Owner
  - The product owner is usually someone on the technology side (though generally not a technologist).  This person is well-versed in communicating between business and technical entities.
  - Responsible for maintaining a prioritized, sane list of work.
  - Takes input from technical stakeholders as well as business stakeholders.
- Scrum Master
  - Facilitator.  Servant leader.  Expert on scrum process.
  - Defends the team from interruptions and distractions.
  - Helps solve blockers.
  - Usually responsible for collecting metrics.
- Contributors
  - Generally not specialists.  T-shaped, not I-shaped.  (See Rituals.Planning)
  - Sufficient skill set to support product holistically.
  - Static.  Too much change frustrates the ability of the team to collect performance metrics.

---

### Stories, Pointing, and Planning

- User Stories
  - They take the form `As a user of FooBank's mobile app, I should be able to log in using the fingerprint reader on my phone`.
  - These can vary quite but, but should take the form `As a ___(who)___, I should be able to ___(do what)___`.  
  - Stories can optionally include metrics (e.g. time, number of clicks) or reasoning (e.g. better security, accessibility).
  - IMPORTANT: Stories are written like this to avoid "solutioning."  For example, a story should not read `Add v1.8.0 of Fingerprintjs to mobile app`.
- Story Pointing
  - Based on the first six of the Fibonacci sequence: 1, 2, 3, 5, 7, 12
  - Sometimes called "T-Shirt Sizing" (e.g. 1=XXS, 2=XS, 3=S, 5=M, 7=L, 12=XL)
  - Can also be labelled as 20, 40, 80, or 100, which indicate stories that need further breakdown (too large for single scrum/iteration)
  - Sizes are arbitrary, relative, and the team must agree on them.
  - Why?
    - Sprint planning (velocity)
    - Backlog planning
- Planning
  - Sprint
    - Pick stories from the top of the (prioritized!) backlog
    - Everything is a COMMITMENT
    - How much to take: velocity <--> sizing
  - Backlog
    - Product owner takes input from stakeholders to prioritize
    - Highly-prioritized stories should be well-understood and pointed (sized)
    - Backlog should be prioritized prior to start of iteration/sprint
    - Can tell approximately when a feature will be done based on the velocity and size of items in the backlog

---

### Velocity

The number of points the team typically completes in an iteration.

---

### Rituals

- Planning
  - ~1hr for a 2 week sprint
  - Swipe from the top of the backlog
  - Ideally, take the top X stories such that `sum(size(X)) <= velocity`
  - If this is not possible (usually due to some amount of specialization), take the top X stories such that the team has an evenly-distributed workload.
- Retrospective
  - Everyone contributes
  - What went well
  - What needs improvement --> action items
  - Review metrics
    - Velocity
    - Commit-to-Accept ratio
  - "Blameless" Postmortems
    - Do not tolerate retribution or shaming for mistakes (and don't confuse "blameless postmortem" for "no ownership or accountability")
    - [How Etsy does it](https://codeascraft.com/2012/05/22/blameless-postmortems/)

Less prescriptive:
- Intake & Grooming
  - This can vary based on the needs of the organization
  - Could be one combined meeting ... could be two separate meetings
    - Example:
    - Intake on week 1 of sprint
    - Grooming on week 2 of sprint
    - Ready to go for start of next sprint
  - Product owner must take input from stakeholders (technical, business, self, etc.)
  - Stakeholders negotiate priorities
  - Product owner might write stories ... team might write stories
  - Important outcomes:
    - Stories in backlog align with broader planning
    - Backlog is prioritized to deliver highest-value work next
    - Stories are understood by team
    - Stories in the backlog (at least the ones near the top) have points assigned
    - Old, unnecessary, or irrelevant stories are discarded

---

### Scrum Board

Most teams use a board to visually track work within a sprint.  The example below is fairly simple.

- Simple example
  - ToDo - User stories that have not been started
  - Doing - User stories that are being worked by the team
  - Done - User stories that are awaiting sign-off by the product owner
  - Accepted - User stories are _really_ done.

At the start of the sprint, the highest-priority stories are brought in from the backlog.

```
+-------------------------------------------+
|  To Do   |  Doing   |   Done   | Accepted |
|----------+----------+----------+----------|
|  Story1  |          |          |          |
|  Story2  |          |          |          |
|  Story3  |          |          |          |
|  Story4  |          |          |          |
|  Story5  |          |          |          |
+----------+----------+----------+----------+
```

During the sprint, the team works the stories (optionally in order of highest- to lowest-priority).  When stories are completed, the product owner "accepts" that the work met the requirements agreed on in the user story.

```
+-------------------------------------------+
|  To Do   |  Doing   |   Done   | Accepted |
|----------+----------+----------+----------|
|          |          |          |  Story1  |
|          |          |          |  Story2  |
|          |          |  Story3  |          |
|          |  Story4  |          |          |
|          |  Story5  |          |          |
+----------+----------+----------+----------+
```

Doesn't it look beautiful?

```
+-------------------------------------------+
|  To Do   |  Doing   |   Done   | Accepted |
|----------+----------+----------+----------|
|          |          |          |  Story1  |
|          |          |          |  Story2  |
|          |          |          |  Story3  |
|          |          |          |  Story4  |
|          |          |          |  Story5  |
+----------+----------+----------+----------+
```

It may be tempting to make a more complicated model, but this can be confusing, prescriptive, and difficult to maintain (even with good tooling).

The simple example workflow looks like this:

`To Do --> Doing --> Done --> Accepted`

A more complicated example might look like this:

`To Do --> Doing --> Deployed to Dev --> Needs QA --> QA'd --> Deployed to Staging --> Needs Accepted --> Ready to Deploy --> Deployed to Prod`

You can see what they're going for.  But keep it as simple as possible.  Instead of making the scrum board match your workflow as closely as possible, make a simple scrum board.  Then make your workflow match it as closely as possible.

---

### The Planning Onion

There's disagreement around this, but I like the metaphor.

![planning_onion.jpeg](planning_onion.jpeg)

Your onion rings don't have to look like these.  They don't have to have the same labels.  You just need to be cognizant of the idea that higher-level planning has a strong relationship to the day-to-day planning.  Not that it _drives_ the day-to-day planning, just that they're related.  There should be strong elements of outside-in planning (e.g. market-driven strategy) and inside-out planning (e.g. technical debt, bug fixes, etc.)

- Staying too focused on the per-day or per-sprint planning can result in a huge amount of churn for the team.
  - A team that jumps from customer feature to customer feature can yield an application that's a complicated mess.  If there's no cohesive, longer-term planning, everything becomes bailing wire and duct tape.
- Staying too focused on the strategic (usually business-side) planning can result in an aimless team, or worse - a frustrated team that feels their technical concerns are not being heard or addressed.
  - An application built around a single database can only scale so far.  If the day-to-day operational challenges are ignored or just hastily patched in favor of an undying commitment to the long-term plan, things will get complicated.

---

### Other Tips

- Remember the [Agile Manifesto](http://agilemanifesto.org/)
- Don't change a process until you understand:
  - Why it is the way it is
  - What you expect to change via a process change
  - How you will measure the change (even if it's informal)
