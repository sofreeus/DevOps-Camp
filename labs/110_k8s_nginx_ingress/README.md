# Kubernetes Nginx Ingress

```
From: Eli <guru@house-of-py.com>
To: Peyton <devops@house-of-py.com>
Subject: Cost Saving Idea

Hello Peyton,

Sarah told me you've got the webapp running on Kubernetes.  That's fantastic and
will serve you well in the future.  You should try out the nginx-ingress for
K8s.  It gives you more straightforward control over your routing and mitigates
some of the cost of load balancers.

-Eli

```

---

#### What are we going to do in this lab?

Leverage the Ingress Controller and Ingress features of Kubernetes to route traffic to our service.

#### Why do we need to do this?

Vendor-provided load balancing options can be both expensive and challenging to manage.  Using a Kubernetes ingress controller can be more cost effective and gives us more control over things like rewrite rules, throttling, and SSL certificates.

#### When do you use it?

Use an ingress when you need to expose a service to consumers outside your K8s cluster.

---

### Make a git commit

Commit early.  Commit often.

This is a good time to take another snapshot of your webapp repository and back it up to the server.

```bash
git add -A
git commit -m 'describe your changes'
git push
```

---

### Switch Service to NodePort

- GKE/GCP doesn't automatically delete the load balancer!
- Delete the load balancer manually
- What's the difference between ClusterIP and NodePort?

- Start with `webapp-service-loadbalancer.yaml`
- Change the service type to `NodePort`
- Add a `name: webport` for your service's port
- Name the file `webapp-service.yaml`
- Get rid of any other `webapp-service*` files

```bash
kubectl apply -f webapp-service.yaml
```

---

### Apply the ingress config

```bash
DEV_MACGUFFIN= #Yours? Your partner's?  Make sure it matches the DNS name.
PROD_MACGUFFIN= #Yours? Your partner's?  Make sure it matches the DNS name.

# Copy the webapp-ingress.yaml in this lab to your repo
cp ~/git/DevOps-Camp/labs/110_k8s_nginx_ingress/webapp-ingress.yaml ~/git/${REPO_NAME}/k8s


sed -i -e "s/DEV_MACGUFFIN/${DEV_MACGUFFIN}/g" webapp-ingress.yaml
sed -i -e "s/MACGUFFIN/${MACGUFFIN}/g" webapp-ingress.yaml

# Apply it to your cluster
kubectl apply -f webapp-ingress.yaml

# Check it out
kubectl get ing

# Open the admin page
xdg-open ${REPO_NAME}.dev.sfsdevops.camp/admin
```
