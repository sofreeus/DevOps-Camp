Company: _Aunt Ettie's House of Py_

- Ettie has moved on, leaving EJ in charge of the company.

- EJ is not terribly technical.  This means that tasks will be defined in terms of business needs.  "Make everyone use git, like this..." is not a good introduction to our labs.  "Our developers are stepping all over each other!  None of them seem to know what the others are doing!" is what we're going for.

- EJ has a strong product & customer focus.  An operations team reporting on server uptime is not as useful as a product team reporting on product availability and/or customer impact.

- Every lab should clearly answer "How does this benefit the product, customer, and company?" in non-technical prose.