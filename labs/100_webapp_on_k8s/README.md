# Apps on Kubernetes

```
From: Sarah <cto@house-of-py.com>
To: Peyton <devops@house-of-py.com>
Subject: Moving Forward

Hi Peyton,

I like the idea that our app would be more portable on Kubernetes.  And it seems
like we'd have less up-front work to do to make that happen.  Let's move forward
on GKE for now, but we'll keep our eyes open for Fuga Cloud's K8s offering in
the future.

Can you get the webapp up and running on Kubernetes?

Thanks,
Sarah

```

---

#### What are we going to do in this lab?

Get our Django webapp running on Kubernetes.

#### Why do we need to do this?

Kubernetes provides a standardized platform for running containers, with features like self-healing and rolling updates.

#### When do you use it?

We configured our webapp to run on an AWS autoscaling group, but it left us with some questions.  Primarily, we ran in to problems because we had to choose:
- Do we dedicate our scaling group to a single service?
- Do we add to the complexity of our management to enable a multi-service cluster?

Container orchestration (Kubernetes) abstracts the infrastructure from our application in a way that enables multi-service clusters.

Simply put, it _actually_ offers "infrastructure as a service."

When would you _not_ use Kubernetes?  We're going to put Postgres on Kubernetes, but you might decide that the features/cost/labor tradeoffs of a managed service (Google's CloudSQL, Amazon's RDS, or something like Mongo Atlas) are worth it.

---

## Prep

Reminder: You and your partner should each have a cluster.  Treat one as DEV and one as PROD.  Use the DEV cluster for the foreseeable future - until advised otherwise.

```bash
# Make a dir for Kubernetes manifests
mkdir ~/git/${REPO_NAME}/k8s

# Copy the manifests from this lab to your project
cp -v ~/git/DevOps-Camp/labs/100_webapp_on_k8s/*.yaml ~/git/${REPO_NAME}/k8s/

# Run this lab from the `k8s` directory
cd ~/git/${REPO_NAME}/k8s

# Make sure your MACGUFFIN is set!
echo ${MACGUFFIN}

# Replace MACGUFFIN in the manifests with your Macguffin name...
sed -i -e "s/MACGUFFIN/${MACGUFFIN}/g" *.yaml

# Verify that your Macguffin shows up in the manifests
grep ${MACGUFFIN} *.yaml

# Make sure you're using your DEV cluster
kubectl config get-contexts
kubectl config rename-context super_long_name_for_student_X dev
kubectl config rename-context super_long_name_for_student_Y prod
kubectl config use dev
kubectl get nodes
```

---

## (Kinda) Persistent storage on Kubernetes

This is the Kubernetes version of the `docker volume`

- Reserve a volume for Postgres
  - `kubectl apply -f postgres-volume-claim.yaml`
- Check it out
  - `kubectl get pvc ${MACGUFFIN}-webapp-db-disk -o yaml`

---

### Secrets

- Ad-hoc generate a password for Postgres
  - `kubectl create secret generic ${MACGUFFIN}-webapp-secrets --from-literal=db_user=postgres --from-literal=db_password=$(pwgen -y 16 1)`
- Verify the values:
  - `kubectl get secret ${MACGUFFIN}-webapp-secrets -o json | jq '.data.db_user' -r | base64 --decode`
  - `kubectl get secret ${MACGUFFIN}-webapp-secrets -o json | jq '.data.db_password' -r | base64 --decode`

---

### PostgreSQL

- Examine the `postgres-*.yaml` files.
- Apply the deployment
  - `kubectl apply -f postgres-deploy.yaml`
- Examine the deployment
  - `kubectl describe deploy ${MACGUFFIN}-postgres`
  - `kubectl get deploy ${MACGUFFIN}-postgres -o yaml`
  - `kubectl get deploy ${MACGUFFIN}-postgres -o json`
- Apply the service
  - `kubectl apply -f postgres-service.yaml`
- Examine the service
  - `kubectl describe service ${MACGUFFIN}-webapp-db`
  - `kubectl get svc ${MACGUFFIN}-webapp-db -o yaml`
  - `kubectl get svc ${MACGUFFIN}-webapp-db -o json`

---

### Webapp

- Examine the `webapp-*.yaml` files
- Apply the deployment
  - `kubectl apply -f webapp-deploy.yaml`
- Apply the ClusterIP version of the service
  - `kubectl apply -f webapp-service-clusterip.yaml`
- Examine these
  - As above

Can you access the ClusterIP service?

- Let's switch to a LoadBalancer service
  - `kubectl apply -f webapp-service-loadbalancer.yaml`
- Doesn't work?
  - `kubectl delete service ${MACGUFFIN}-webapp` first
- Wait for the load balancer creation
  - `kubectl get svc --watch`

---

### Initialize the database

```bash
# Examine webapp-init-job.yaml
# Apply it to the cluster
kubectl apply -f webapp-init-job.yaml

# Check the status
INIT_POD=$(
  kubectl get pod -o jsonpath="{.items[?(@.metadata.labels.job-name==\"${MACGUFFIN}-webapp-init\")].metadata.name}"
)

kubectl logs ${INIT_POD}

# Clean it up (because it's done)
kubectl delete job webapp-init

# Create the superuser
WEBAPP_POD=$(
  kubectl get pod -l app=webapp,tier=web,macguffin=${MACGUFFIN} -o jsonpath='{.items[].metadata.name}'
)

kubectl exec -it ${WEBAPP_POD} python manage.py createsuperuser
```

---

### Get your partner access to your cluster

#### Give your partner permissions to use Kubernetes in your project
- Go to the GCP console
- Select your project
- Use the hamburger menu to open `IAM & admin`
- Find your partner's e-mail address on the page.  Copy it.
- Click the `ADD` button
- Paste in your partner's e-mail address
- Select `Kubernetes Engine` --> `Kubernetes Engine Admin` for the role
- Save.

#### Give your partner the connection command to your cluster
- Use the hamburger menu to open `Kubernetes Engine`
- Click the `Connect` button next to your cluster
- Copy the connection string
- Give it to your partner

#### Contexts

```bash
# List contexts
kubectl config get-contexts

# Rename contexts
kubectl config rename-context <current_name> <new_name>

# Switch contexts
kubectl config use <target_context>
```
