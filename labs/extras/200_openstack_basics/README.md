# OpenStack

OpenStack is a free-libre datacenter software for provisioning private and public cloud infrastructure. It's sort of like AWS, GCP, or Azure, but it's open-source, like Linux.

## Create a new project

Get the admin password to the OpenStack cluster from an instructor, or ask the instructor to perform the following steps for you.

1. Sign in to [Horizon](http://delimiter.sofree.us), the OpenStack Dashboard, as 'admin'.
*  Download admin's RC file. Click the user control in the upper-right corner, and choose "OpenStack RC File v3".
*  Create the new project in the CLI:
  1. Build a `project.settings.private` file from a project.settings.(example).
  2. Source admin's RC file.
  3. Run `admin.create-project project.settings.private` to create a new project.
*  Verify project, users, network, and router in the GUI or CLI.

## Create a new server

Sign in to Horizon as any of the project users just created and create an instance GUI'ly.

Project > Compute > Instances > Launch instance
  * Details:
    + Instance name = 'alpha'
  * Source:
    + Delete volume... = yes
    + Choose the "Cirros..." image
  * Flavour:
    + Choose "m1.tiny"

## Allocate two Floating IPs

Project > Network > Floating IPs

Click the "Allocate IP To Project" button, then click "Allocate IP". Do that twice.

Click "Associate" on one of the addresses and associate it with one of the Instances.

Project > Compute > Instances

Note that one of the instances has an Floating IP.

On the other instance, click the down-pointing triangle near the "Create Snapshot" button, and choose "Associate Floating IP"

## Create a public ssh security group and a public ping security group

Project > Network > Security Groups

Create two Security Groups. One named "public ping" and one named "public ssh".

Click "Manage Rules" on the "public ping" security group and add a rule to allow "All ICMP" from everywhere (0.0.0.0/0, the default).

Start to ping one of the Floating IPs in a terminal session.

Project > Compute > Instances

On the instance with the Floating IP you're pinging, click the down-pointing triangle to the right of Create Snapshot, and choose Edit Security Groups. Add the 'public ping' Security Group to this insance. Note that as soon as you save the change, the ping begins to succeed.

Project > Network > Security Groups

Edit the 'public ssh' Security Group to allow SSH, either public, or just your and your partner's IP addresses.

Project > Compute > Instances

Add the "public ssh" Security Group to the other instance with a Floating IP.

Verify that you can ssh to the Floating IP.
