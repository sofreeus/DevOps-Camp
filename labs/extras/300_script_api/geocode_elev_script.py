#!/usr/bin/env python

import requests, os

# Import the API key from the OS environment variables
mq_api_key = os.environ['MQ_API_KEY']

# Here's all my geocoding request data in a nice, readable format
addr='1101 County Rd 53, Granby, CO 80446'
geocode_url = 'https://www.mapquestapi.com/geocoding/v1/address'
args = { 'key': mq_api_key, 'location': addr }

# We make the request.  Like `curl` in the shell, but we store the result
response = requests.get( geocode_url, params=args )

# Decode the JSON result to a Python dictionary for easy access
geocode_data = response.json()

# Pull the geo data out of the response
# (It takes up-front work - maybe in the shell - to get this right.)
lat_lng = geocode_data['results'][0]['locations'][0]['latLng']
lat = lat_lng['lat']
lng = lat_lng['lng']

# Here's all my elevation request data in a nice, readable format
elevation_url = 'http://open.mapquestapi.com/elevation/v1/profile'
args = { 'key': mq_api_key, 'latLngCollection': f"{lat},{lng}", 'unit': 'f' }

# Make the request to the elevation endpoint and store the response
response = requests.get( elevation_url, params=args )

# Decode the JSON result to a Python dictionary for easy access
elevation_data = response.json()

# Grab the height from the response
elev = elevation_data['elevationProfile'][0]['height']

print( f"The elevation is {elev} feet" )
