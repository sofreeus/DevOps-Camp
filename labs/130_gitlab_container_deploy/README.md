# Deploy from CI

```
From: Ada <development@house-of-py.com>
To: Peyton <devops@house-of-py.com>
Subject: Deploys?

Hi Peyton,
Since you set the app up to build in Gitlab CI, can you also make it deploy?  I
could ask my devs to write their own Kubernetes manifests, but I'm not sure you
want that.  :p

Thanks,
Ada

```

---

#### What are we going to do in this lab?

Why stop at generating an image?  Use GitLab CI to deploy a running app to Kubernetes.

#### Why do we need to do this?

We want to automate the full pipeline, and this is part of the pipeline.  Automating the deploy opens the door to continuous delivery - and even continuous deployment!

#### When do you use it?

Use this as a start toward developing your own standardized deployment process from your CI system.  Ideally, all applications at your organization will follow a similar - if not the same - deployment process.

---

### Prep - Reorganize

Add this to your `~/git/management_scripts/repo_setup.sh`

Quoted lines for context/location.

> else
>
>  echo "${PROJECT_NAME} created successfully."
>
>fi

```bash
SSH_URL=$(echo ${RESPONSE} | jq -r '.ssh_url_to_repo')
mkdir -p ~/git/
git clone ${SSH_URL} ~/git/${PROJECT_NAME}
```

> `#` Share our new projects with our subgroups:

Now you don't have to go hunt for the ssh_url_to_repo...

```bash
# Create a new repository for your Postgres service
# This should look like: ${MACGUFFIN}-webapp-postgres
~/git/management_scripts/repo_setup.sh ${REPO_NAME}-postgres

# Move your Postgres bits into that repo
mkdir ~/git/${REPO_NAME}-postgres/k8s
mv ~/git/${REPO_NAME}/k8s/postgres* ~/git/${REPO_NAME}-postgres/k8s
cp ~/git/DevOps-Camp/labs/130_gitlab_container_deploy/postgres.gitlab-ci.yml \
   ~/git/${REPO_NAME}-postgres/.gitlab-ci.yml

# Commit and push your postgres repo
cd ~/git/${REPO_NAME}-postgres
git add -A
git commit -m 'add k8s manifests'
git push
```

Let's get the init job out of the way so CI doesn't try to re-run it.

```bash
mkdir ~/git/${REPO_NAME}/jobs
mv ~/git/${REPO_NAME}/k8s/webapp-init-job.yaml ~/git/${REPO_NAME}/jobs
```

- Make sure the Kubernetes stuff in your *macguffin-webapp* looks like this  
  ```
  k8s
  ├── webapp-deploy.yaml
  ├── webapp-ingress.yaml
  └── webapp-service.yaml
  jobs
  └── webapp-init-job.yaml
  ```

---

### Create a service account for Gitlab

This enables Gitlab to perform deploys on your cluster.

1. cloud.google.com --> console --> IAM --> Service Accounts
1. Create Service Account --> `gitlab-deploy`
1. Role = `Kubernetes Engine Developer`
1. Check the `Furnish a new private key` checkbox; select JSON
1. Save

---

### Configure GitLab Environment Variables

###### Set group-wide variables
- Go to GitLab --> your GROUP (not repo) --> Settings --> CI/CD --> Variables
- Add an authentication variable
  - `DEV_GCP_AUTH_KEYFILE`
  - Paste the *entire* contents of the JSON file (you just downloaded) into the value field.
- While you're here, add the following variables as well...
  - Set each of these to the corresponding values for your cluster
  - `DEV_CLUSTER`
  - `DEV_PROJECT`
  - `DEV_REGION`
- Save variables

###### Set per-repo variable(s)
- Go to GitLab --> your REPO --> Settings --> CI/CD --> Variables
  - Add a host variable
    - `DEV_SERVICE_URL` = Your sfsdevops.camp or house-of-py.com host.  FQDN.  No protocol.

---

### A Deploy Token for Kubernetes

This enables Kubernetes to read from your registry.

*If you still have your deploy token from earlier, you can skip this block*
1. Gitlab --> your webapp repository --> Settings --> Repository --> Deploy Tokens
  1. Name it `${MACGUFFIN}-cluster`
  1. Grant it `read_registry` scope
  1. Click `Create deploy token`
  1. Store the resulting credentials in Keypass

*But don't skip this*

Create a secret on your cluster

```bash
kubectl create secret docker-registry ${REPO_NAME}-registry \
--docker-server=registry.gitlab.com \
--docker-username="USERNAME_GOES_HERE" \
--docker-password="PASSWORD_GOES_HERE" \
--docker-email="does.not@matt.er"
```

Reference this secret in your deploy by adding this block to the 
`.spec.template.spec` in `webapp-deploy.yaml`

```yaml
imagePullSecrets:
- name: ${REPO_NAME}-registry
```

---

### Modify your Manifests

```bash
# Replace the Docker image in the deploy
#   image: registry.gitlab.com/sofreeus/devopscamp-webapp:latest
# with
#   image: DOCKER_IMAGE
atom ~/git/${MACGUFFIN}-webapp/k8s/webapp-deploy.yaml

# Replace the hard-coded URL in the ingress
# It should read:
#   - host: SERVICE_URL
atom ~/git/${MACGUFFIN}-webapp/k8s/webapp-ingress.yaml
```

---

### Make it work!

```bash
# Replace the old `.gitlab-ci.yml` with the one from this lab
cp -v ~/git/DevOps-Camp/labs/130_gitlab_container_deploy/.gitlab-ci.yml ~/git/${REPO_NAME}/

# Examine the file
atom ~/git/${REPO_NAME}/.gitlab-ci.yml

# Commit!
# Push!
cd ~/git/${REPO_NAME}
git add -A
git commit -m 'update my gitlab CI configuration'
git push
```
