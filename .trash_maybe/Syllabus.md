---

- Machine setup
    - Virtualbox?
    - Docker
- API scripting
    - bash + awk
    - bash + jq
    - python + requests
- Git
    - Basic introduction
        - SSH key
        - clone/add/commit/push
    - API interaction
        - API Token
        - Create a repo
- Docker
    - Hello World
    - Build our own webserver
    - Django multi-tier example
        - https://docs.docker.com/compose/django/
- Kubernetes setup
    - GCP/GKE
    - AWS/EKS
    - K8s Dashboard
- K8s applications
    - GitLab
        - Scale CI worker nodes
    - Docker registry
    - Django
    - Sensu
    - ELK+Grafana
        - Scale Elasticsearch
        - Redis + Scale Logstash?
        - What do we monitor?
- CI/CD Pipelines
    - Build
    - Test
    - Deploy
- Configuration Management
    - Ansible for admin SSH keys on EKS worker nodes?
- Organizational

---
    
- Stitch together disparate applications with API scripting
- Do more with less; manage your infrastructure as code
- Use containers for developer freedom and operational consistency
- Scale out simply with orchestration tools that can eliminate maintenance windows
- Build a decentralized, self-service monitoring infrastructure
- Leverage your DevOps skill set to build a continuous integration (CI/CD) pipeline


- API Scripting
    - bash
        - Using jq to handle JSON
    - python
        - Handling JSON the easy way

- Version Control
    - git
    - GitLab
    - The best way for teams to interact in a central repository
    - Scripting repository setup

- Infrastructure as Code
    - HEAT templates?
        - Networking
            - VPC
            - Subnets
            - Routes
            - Gateways
            - DNS
        - Security
            - Security groups
            - User/Role/Profile
        - Instances
        - Scaling groups

- Configuration Management
    - Ansible

- Architecting for Scale
    - Immutable infrastructure
    - Load balancing

- Containers
    - Docker
        - Public containers as building blocks
        - Local prototyping
        - Building
        - Optimizing
    - Kubernetes
        - Minikube
        - GKE?

- Continuous Integration
    - GitLab CI

- Organizational Considerations
    - Product focus
    - Collaboration
    - Service approach

---

Day 1
- Set up git repo
- Infrastructure as code
    - VPC
    - Subnets
    - Routes
    - Gateways
    - Security groups
    - Instances
    - Scaling groups
    - DNS entries

Day 2
- Sensu in Docker?
    - Monitoring
    - API scripting
    - Docker
- Configuration Management?

Day 3
- CI/CD Pipelines
    - Build
    - Test
    - Deploy

Day 4
- Docker containers in the cloud...
    - Kubernetes?  Minikube?  Murano?
    - Swarm?
    - Mesos + Marathon?

---

- Start with students running Git from a fileserver.
- Silvia teaches HEAT templates to provision infrastructure for K8s.
- Set up K8s on OpenStack.
- Deploy GitLab as a K8s app, then migrate repos.
- Implement a demo app in GitLab+K8s, via GitLab CI
- Add a monitoring solution (Sensu if not Prometheus)

---

## Out of scope

- ELK Stack
- AWS/GCE/DO proprietary extensions
