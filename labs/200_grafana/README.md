# Grafana

```
From: Ada <development@house-of-py.com>
To: Sarah <cto@house-of-py.com>; Peyton <devops@house-of-py.com>
Subject: Team Status Dashboards

Sarah,
The metrics collection that Peyton set up is pretty killer.  I'd really like to
put the metrics up somewhere everyone can see them.  Can we order a TV for the
engineering area?

Peyton,
What tool can we use for dashboards?  We've got all the metrics, but we need a
way to display them.

Thanks!
Ada

```

---

Grafana is a dashboard tool that can display data from a variety of sources.  We're going to set it up and point it at Prometheus.

- Create a `${MACGUFFIN}-grafana` repository
- Add environment variables for `DEV_ADMIN_PASSWORD` and `PROD_ADMIN_PASSWORD`
- Copy the contents of the this repo
- Make a commit and push to your repo

---

### Prometheus Data Source

- Access Grafana
  - `xdg-open https://grafana.dev.${MACGUFFIN}.sfsdevops.camp`
- Use the wizard to add a data source
  - Name: `${MACGUFFIN}-prometheus`
  - Type: `Prometheus`
  - URL: `http://${MACGUFFIN}-prometheus-server:80`
  - Basic Auth: `checked`
  - User: `admin`
  - Password: Pull from Gitlab env vars or from Keepass
- Save & Test!
- Scroll to the top and click the `Dashboards` tab
- Import `Prometheus 2.0 Stats` (and others, if you want)
- Mouse-over the dashboards icon on the left-nav menu and click `Manage`
- Click `Prometheus 2.0 Stats`
- Celebrate!
