#!/usr/bin/env python

import requests, os

mq_api_key = os.environ['MQ_API_KEY']


def main():
    lat,lng = geocode()
    elev = elevation(lat,lng)
    print( f"The elevation is {elev} feet" )


def geocode(addr='1101 County Rd 53, Granby, CO 80446'):
    # Here's all my data in a nice, readable format
    geocode_url = 'https://www.mapquestapi.com/geocoding/v1/address'
    args = {
        'key': mq_api_key,
        'location': addr
    }

    # We make the request.  Like `curl` in the shell, but we store the result
    response = requests.get( geocode_url, params=args )

    # Decode the JSON result to a Python dictionary for easy access
    geocode_data = response.json()

    # Pull the geo data out of the response
    # (It takes up-front work - maybe in the shell - to get this right.)
    latitude = geocode_data['results'][0]['locations'][0]['latLng']['lat']
    longitude = geocode_data['results'][0]['locations'][0]['latLng']['lng']

    # Return the results
    return [ latitude, longitude ]


def elevation(x=40.004652,y=-105.919627):
    # What is this?
    elevation_url = 'http://open.mapquestapi.com/elevation/v1/profile'
    args = {
        'key': mq_api_key,
        'latLngCollection': f"{x},{y}",
        'unit': 'f'
    }
    # What are we doing?
    response = requests.get( elevation_url, params=args )
    # How about here?
    elevation_data = response.json()
    # WHY???
    return elevation_data['elevationProfile'][0]['height']


main()
