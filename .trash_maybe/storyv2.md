  Your name is *Peyton*, and you just started work at _Aunt Ettie's House of Py_, a fledgling subsidiary of _Aunt Ettie's House of Pie_.  Your title is "DevOps Lead," but the title doesn't really matter; there are no other DevOps people to lead.  This is a small organization and you're going to be very hands-on with a broad set of responsibilities.

  *Aunt Ettie* has employed some fine nepotism to set up *EJ* as the *CEO* of _House of Py_, the tech startup that will bring _House of Pie_ online.  EJ is excited, enthusiastic, demanding, and not terribly technical.

  *Sarah* is the *CTO*.  She's been trying to implement Agile within the organization, but things aren't going as smoothly as she wants.

  *Ada* is the *manager of the software team*.  She is very hands-on with the technology because the team is so small.

  *Crumb* is the *product owner* for the team, and fits as a communication hub between EJ, technology, and customers.  Crumb has a first name, but you don't know what it is.

  *Justin* is the *Systems Administrator*, and the only person at the company representing anything like an Operations group.

  *Eli* is a prospective board member who wrote a best-selling novel about technology process optimization, _The Fawkes Fenomenon_.  He's a busy guy, but a *good resource* when you can get him.
