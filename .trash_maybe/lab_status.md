|  **Lab** | **Topics** | **Status** | **Dependencies** |
|  ------ | ------ | ------ | ------ |
|  devops, agile, & culture | - holistic approach to product | Needs written | None |
|   | - fostering the adoption of change |  |  |
|   | - how to stretch your boundaries and be the example |  |  |
|  api | - bash | Tech written; needs story | None |
|   | - bash + jq |  |  |
|   | - python |  |  |
|  gitlab | - SSH key | Needs updating from IntroToDevOps | None |
|   | - API token |  |  |
|   | - Using the API to create a repo |  |  |
|   | - git clone |  |  |
|   | - git add/commit/push |  |  |
|  openstack | - Users | Needs written | None (but...) |
|   | - Projects |  |  |
|   | - Networking |  |  |
|   | - Load balancing |  |  |
|   | - Scaling groups |  |  |
|   | - Templating/bootstrapping instances |  |  |
|  ansible | - install | Need to steal from Shoup | OpenStack |
|   | - configure |  |  |
|   | - deploy admin ssh keys to openstack instances |  |  |
|  docker | - Using existing stuff | Needs updating from IntroToDevOps | None |
|   | - Volumes |  |  |
|   | - Ports |  |  |
|   | - Networks |  |  |
|   | - Building your own |  |  |
|   | - Dockerfiles |  |  |
|   | - Optimization |  |  |
|   | - Image management |  |  |
|  docker + openstack | - Running containers on the OS | Needs written; some might be able to be pulled from IntroToDevOps | OpenStack |
|   | - Demonstrate a viable multi-service app |  |  |
|   | - Music stack?  Guest book?  SENSU???? |  |  |
|   | - Explore failure conditions |  |  |
|  kubernetes | - Installation? | Needs written; some might be able to be pulled from Marcial's class | OpenStack |
|   | - Configuration? |  |  |
|   | - Roles??? |  |  |
|   | - Services |  |  |
|   | -Deployments |  |  |
|   | - Services |  |  |
|   | - Ingresses |  |  |
|   | - Storage |  |  |
|   | - StorageClasses |  |  |
|   | - PersistentVolumeClaims |  |  |
|  docker + k8s | - Move the multi-service app to k8s | Needs written | None to write, OpenStack to teach |
|   | - Explore failure conditions |  |  |
|  continuous_integration | - Use GitLabCI to build/test our multi-tier app | Needs written; most should be able to be pulled from IntroToDevOps | None to write, OpenStack to teach |
|   | - Deploy to OS |  |  |
|   | - Deploy to k8s |  |  |
|  elk | - Use CI w/ IaC to deploy ELK to k8s | Needs written | None to write, OpenStack to teach |
