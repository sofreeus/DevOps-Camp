EXTRA Packing
=============

See also:
- [normal packing](https://gitlab.com/sofreeus/sofreeus/blob/master/packing.md)
- [student packing](student-packing.md)

    Print cheat-sheets
    ISO's for distributions used
    Network switch, wireless bridge, long cables, cable-ties
    Presenter's laptop, server, caching web-proxy
    Spare laptops
    Games and Toys
    - frisbee
    - blocks
    - pyramids
    - zometool
    - buckyballs
    Books: Continuous Delivery, etc...
    Recycling bins
    Notepads
    Whiteboard and big stickies
    Peace, Love, and Linux woods
