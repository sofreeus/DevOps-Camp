# Adding TLS/SSL on your ingress

```
From: Sarah <cto@house-of-py.com>
To: Peyton <devops@house-of-py.com>; Ada <development@house-of-py.com>
Subject: In-Transit Encryption

Hey y'all,
We should get in front of the eight ball on security.  There's no reason we
can't use in-transit encryption ... pretty much everywhere.  Please set up SSL
on the Kubernetes cluster, so we can use it with any app.

Thanks,
Sarah

```

---

#### What are we going to do in this lab?

We're going to learn how to secure our web-facing properties with TLS certificates that automatically renew via LetsEncrypt.

#### Why do we need to do this?

Starting in Chrome 68, the browser is warning users whenever they go to a non-TLS site.  This is table stakes.  TLS (f.k.a. SSL) ensures that communications between the client and ingress are encrypted.  Without it, information like passwords are transmitted in plain text.

#### When do you use it?

There is no decision point here.  Us it everywhere all the time.  The initial setup is a challenge, so you should work to add it to your cluster provisioning automation.  When you've got it included in the cluster by default, it should be the standard practice to configure every new service with TLS before it gets to production.

---

## Add a Let's Encrypt certificate manager to your cluster

!!! Note: Let's Encrypt has a hard limit of 20 certificates per week per domain (sfsdevops.camp in this case).  Make _absolutely_ sure that you have your certificate working correctly in staging before switching to prod.  If you'd like to buy your own domain to use with GCP, we could do that...

```bash
export NOW=$(date +%Y%m%d%H%M%S)

# Re-elevate your account
kubectl create clusterrolebinding cluster-admin-binding-${NOW} \
  --clusterrole cluster-admin \
  --user $(gcloud config get-value account)

# Install cert-manager
CERT_MANAGER_MANIFEST="https://raw.githubusercontent.com/jetstack/cert-manager/release-0.4/contrib/manifests/cert-manager/with-rbac.yaml"
curl ${CERT_MANAGER_MANIFEST} | kubectl apply -f -

# Remove the admin access for your account
kubectl delete clusterrolebinding cluster-admin-binding-${NOW}
```

---

#### Note

We've been learning Kubernetes in bits and pieces.  Because of this, there is no central repo with scripts and YAML's to set up a whole K8s cluster.  If you are interested in moving forward with K8s on your own, I highly recommend creating a repo for your Kubernetes setup materials.

---

#### Configure the ClusterIssuer resource for staging

Note: This requires that you have DNS properly configured for your cluster.

```bash
cd ~/git/DevOps-Camp/labs/160_tls_ssl

# Make sure you're where you want to be
kubectl config current-context

# Apply the staging certificate issuer to your cluster
kubectl apply -f issuer-staging.yaml

# Check the logs for cert-manager to see what's going on:
kubectl -n cert-manager get pod
kubectl -n cert-manager logs -f WHATEVER_POD
```

---

## Add TLS to your site

Note: You can make changes and try to push them through CI, but that can be a very slow/tedious process.  My recommendation is to prototype locally to iterate more quickly.  When you think you have a working prototype, try to push it out via CI.

#### Add the TLS cert to your ingress

Note: We are using the 'ingress-shim' feature of cert-manager.  You may optionally [define a certificate separately](https://cert-manager.readthedocs.io/en/latest/reference/certificates.html) (on your own time).

```bash
#Get a copy of your running ingress
kubectl get ing demo-webapp -o yaml > /tmp/ingress.yaml
```

We're going to use `/tmp/ingress.yaml` for our prototyping, then add our changes back to the template when we're good.

Add this to the `.metadata` block of your ingress manifest:

```yaml
annotations:
  certmanager.k8s.io/cluster-issuer: letsencrypt-staging
  kubernetes.io/tls-acme: "true"
```

Add this to the `.spec` block of your ingress manifest, and apply it.

```yaml
tls:
- hosts:
  - YOUR_WEBAPP_URL
  secretName: ${MACGUFFIN}-dev-tls
```

It can take >10 minutes for your certificate to verify.  You can watch the cert-manager log to get some insight.

When the staging cert is A-OK, you'll see this in your browser:

![Functional Staging Certificate](working_staging_cert.png)


#### Switch to a real certificate

- `kubectl apply -f issuer-prod.yaml`
- In the `.metadata.annotations` of your ingress, change `letsencrypt-staging` to `letsencrypt-prod`
- Check the cert-manager logs again to watch for the cert.
Check the cert itself to make sure it's using the correct issuer
  - `kubectl get cert ${MACGUFFIN}-dev-tls -o yaml`
  - You may need to delete the secret and let it re-create
    - `kubectl delete secret ${MACGUFFIN}-dev-tls`

You can also use `kubectl get secrets --watch`

---

## Get it through CI

Add the `annotations` and `tls` entries into your ingress template (making sure to change the hostname to `SERVICE_URL`).  Then push your changes and monitor the process in the CI/CD Pipeline.
